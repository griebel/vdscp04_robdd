/**
 * \class Manager
 *
 * \brief A management class so find the ROBDD of a boolean function
 *
 * This class models ROBDDS by using boolean values and if-then-else operations.
 *
 * \author Oliver Griebel
 * \author Luis Humberto Nomura Quiroz
 */
#ifndef MANAGER_HPP
#define MANAGER_HPP

#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

typedef unsigned int BDD_ID;

class Manager {
public:
	Manager(void);
	~Manager(void);
	BDD_ID createVar(const std::string &label);
	BDD_ID True(void) const;
	BDD_ID False(void) const;
	bool isConstant(const BDD_ID bdd_id);
	bool isVariable(const BDD_ID bdd_id);
	BDD_ID topVar(const BDD_ID bdd_id);
	BDD_ID coFactorTrue(const BDD_ID subtree, BDD_ID var);
	BDD_ID coFactorFalse(const BDD_ID subtree, BDD_ID var);
	BDD_ID coFactorTrue(const BDD_ID subtree);
	BDD_ID coFactorFalse(const BDD_ID subtree);
	BDD_ID ite(const BDD_ID bdd_id_i, const BDD_ID bdd_id_t,
			const BDD_ID bdd_id_e);
	std::string getTopVarName(const BDD_ID bdd_id);
	BDD_ID neg(const BDD_ID a);
	BDD_ID and2(const BDD_ID a, const BDD_ID b);
	BDD_ID or2(const BDD_ID a, const BDD_ID b);
	BDD_ID xor2(const BDD_ID a, const BDD_ID b);
	BDD_ID nand2(const BDD_ID a, const BDD_ID b);
	BDD_ID nor2(const BDD_ID a, const BDD_ID b);
	void findNodes(const BDD_ID &root, std::set<BDD_ID> &notes_of_root);
	void findVars(const BDD_ID &root, std::set<BDD_ID> &vars_of_root);
	std::string printBDD_ID(const BDD_ID bdd_id);

private:
	bool isTerminalCase(BDD_ID bdd_id_i, BDD_ID bdd_id_t, BDD_ID bdd_id_e,
			BDD_ID &bdd_id_terminalCase);
	bool isComputed(const BDD_ID i, const BDD_ID t, const BDD_ID e,
			BDD_ID &bdd_id_terminalCase);
	void addComputed(const BDD_ID i, const BDD_ID t, const BDD_ID e,
			const BDD_ID id);
	void checkBDD_IDRange(const BDD_ID bdd_id);
	BDD_ID makeNode(const std::string &label, BDD_ID bdd_id_high,
			BDD_ID bdd_id_low);
	BDD_ID topVar(const BDD_ID bdd_id_f, const BDD_ID bdd_id_g,
			const BDD_ID bdd_id_h);
	BDD_ID topVar(const std::string &label);
	BDD_ID highVar(const BDD_ID bdd_id);
	BDD_ID lowVar(const BDD_ID bdd_id);

	struct tripple {
		std::string top;
		BDD_ID high;
		BDD_ID low;
	};

	struct Key {
		BDD_ID top;
		BDD_ID high;
		BDD_ID low;
	};

	struct KeyHash {
		std::size_t operator()(const Key& k) const {
			return std::hash<BDD_ID>()(k.top)
					^ (std::hash<BDD_ID>()(k.high) << 1)
					^ (std::hash<BDD_ID>()(k.low) << 2);
		}
	};

	struct KeyEqual {
		bool operator()(const Key& lhs, const Key& rhs) const {
			return lhs.top == rhs.top && lhs.high == rhs.high
					&& lhs.low == rhs.low;
		}
	};

	std::map<BDD_ID, tripple> uniqueTable;
	std::unordered_map<Key, BDD_ID, KeyHash, KeyEqual> computedTable;
};

#endif /* MANAGER_HPP */
