/**
 * \brief ‫Implements methods of the Manager class
 *
 * \author Oliver Griebel
 * \author Luis Humberto Nomura Quiroz
 */
#include "Manager.hpp"

////////////////////////////////////////////////////////////////////////////////////
/// Constructor of the manager class.
////////////////////////////////////////////////////////////////////////////////////
Manager::Manager(void) {
	uniqueTable.clear();
	makeNode("0", False(), False());
	makeNode("1", True(), True());
}

////////////////////////////////////////////////////////////////////////////////////
/// Destructor of the manager class.
///
///
////////////////////////////////////////////////////////////////////////////////////
Manager::~Manager(void) {
	uniqueTable.clear();
}

////////////////////////////////////////////////////////////////////////////////////
/// Function for inserting a new variable.
///
/// \param label Name of the variable to create.
/// \return unique id of new variable. 0 means the variable exists already.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::createVar(const std::string &label) {
	BDD_ID bdd_id;
	std::map<BDD_ID, tripple>::iterator iterator;

	if (label.compare("") == 0) {
		return (BDD_ID) 0;
	}

	bdd_id = makeNode(label, True(), False());

	return bdd_id;
}

////////////////////////////////////////////////////////////////////////////////////
/// Function for getting the unique id of true.
///
/// \return unique id of true.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::True(void) const {

	return ((unsigned int) 2);

}

////////////////////////////////////////////////////////////////////////////////////
/// Function for getting the unique id of false.
///
/// \return unique id of false.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::False(void) const {

	return ((unsigned int) 1);

}

////////////////////////////////////////////////////////////////////////////////////
/// Function for identifying, if a id either is or isn't a constant.
///
/// \param bdd_id Unique bdd_id which is getting checked.
/// \return Returnvalue is true, if the bdd_id is a constant. Otherwise the value is false.
////////////////////////////////////////////////////////////////////////////////////
bool Manager::isConstant(const BDD_ID bdd_id) {
	bool constant;

	checkBDD_IDRange(bdd_id);

	constant = false;

	if (bdd_id == True() || bdd_id == False()) {
		constant = true;
	}

	return constant;
}

////////////////////////////////////////////////////////////////////////////////////
/// Function for identifying, if a id either is or isn't a variable.
///
/// \param bdd_id Unique id which is getting checked.
/// \return Returnvalue is true, if the bdd_id is a variable. Otherwise the value is false.
////////////////////////////////////////////////////////////////////////////////////
bool Manager::isVariable(const BDD_ID bdd_id) {
	bool variable;

	checkBDD_IDRange(bdd_id);

	variable = false;

	if (highVar(bdd_id) == True() && lowVar(bdd_id) == False()) {
		variable = true;
	}

	return variable;
}

////////////////////////////////////////////////////////////////////////////////////
/// Function is searching the bdd_id of the top variable.
///
/// \param bdd_id Unique id which is getting checked.
/// \return Id of the top variable. 0 means error
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::topVar(const BDD_ID bdd_id) {
	return topVar(getTopVarName(bdd_id));
}

////////////////////////////////////////////////////////////////////////////////////
/// Function is searching the bdd_id when the CoFactor is True. Taking the "Then" part of a bdd_id.
///
/// \param subtree Unique id which is getting checked.
/// \param var is for comparing the subtree top variable.
/// \return Id of the top variable's "then" part.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::coFactorTrue(const BDD_ID subtree, BDD_ID var) {
	if (isConstant(subtree)) {
		return subtree;
	}

	if (!isVariable(var)) {
		var = topVar(var);
	}

	if (topVar(subtree) == var) {
		return highVar(subtree);

	} else if (topVar(subtree) > var) {
		return subtree;

	} else {
		BDD_ID TE = coFactorTrue(highVar(subtree), var);
		BDD_ID FE = coFactorTrue(lowVar(subtree), var);

		return ite(topVar(subtree), TE, FE);
	}
}
////////////////////////////////////////////////////////////////////////////////////
/// Function is searching the bdd_id when the CoFactor is False. Taking the "Else" part of a bdd_id.
///
/// \param subtree Unique id which is getting checked.
/// \param var is for comparing the subtree top variable.
/// \return Id of the top variable's "else" part.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::coFactorFalse(const BDD_ID subtree, BDD_ID var) {
	if (isConstant(subtree)) {
		return subtree;
	}

	if (!isVariable(var)) {
		var = topVar(var);
	}

	if (topVar(subtree) == var) {
		return lowVar(subtree);
	} else if (topVar(subtree) > var) {
		return subtree;
	} else {
		BDD_ID TE = coFactorFalse(highVar(subtree), var);
		BDD_ID FE = coFactorFalse(lowVar(subtree), var);

		return ite(var, TE, FE);
	}
}

////////////////////////////////////////////////////////////////////////////////////
/// Function to get the high cofactor of a node.
///
/// \param bdd_id Unique id which is containing the cofactor.
/// \return BDD_ID of the high cofactor.
///////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::coFactorTrue(const BDD_ID bdd_id) {

	return highVar(bdd_id);

}

////////////////////////////////////////////////////////////////////////////////////
/// Function to get the low cofactor of a node.
///
/// \param bdd_id Unique id which is containing the cofactor.
/// \return BDD_ID of the low cofactor.
///////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::coFactorFalse(const BDD_ID bdd_id) {

	return lowVar(bdd_id);

}

////////////////////////////////////////////////////////////////////////////////////
/// Function return the If-and-else node.
///
/// \param i is the node which contains the top variable
/// \param t is the node connected to the 1 edge or "then"
/// \param e is the node connected to the 0 edge or "or else" node.
/// \return Name of the top variable.
///////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::ite(const BDD_ID i, const BDD_ID t, const BDD_ID e) {
	BDD_ID result;

	if (isTerminalCase(i, t, e, result)) {
		return result;

	} else if (isComputed(i, t, e, result)) {
		return result;

	} else {
		BDD_ID topVariable = topVar(i, t, e);

		BDD_ID I_t = coFactorTrue(i, topVariable);
		BDD_ID T_t = coFactorTrue(t, topVariable);
		BDD_ID E_t = coFactorTrue(e, topVariable);
		BDD_ID TE = ite(I_t, T_t, E_t);

		BDD_ID I_f = coFactorFalse(i, topVariable);
		BDD_ID T_f = coFactorFalse(t, topVariable);
		BDD_ID E_f = coFactorFalse(e, topVariable);
		BDD_ID FE = ite(I_f, T_f, E_f);

		if (TE == FE) {
			return TE;
		}

		BDD_ID result = makeNode(getTopVarName(topVariable), TE, FE);

		return result;
	}
}

////////////////////////////////////////////////////////////////////////////////////
/// Function compute the NOT function for a Statement.
///
/// \param a Unique id which is invalid.
/// \return The NOT function of a statement.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::neg(const BDD_ID a) {
	return ite(a, False(), True());
}

////////////////////////////////////////////////////////////////////////////////////
/// Function compute the AND function for two Statements.
///
/// \param a is th node's bdd_id
/// \param b is th node's bdd_id
/// \return The AND function of two statement.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::and2(const BDD_ID a, const BDD_ID b) {
	return ite(a, b, False());
}

////////////////////////////////////////////////////////////////////////////////////
/// Function compute the OR function for two Statements.
///
/// \param a node's bdd_id.
/// \param b node's bdd_id.
/// \return The OR function of two statement.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::or2(const BDD_ID a, const BDD_ID b) {
	return ite(a, True(), b);
}

////////////////////////////////////////////////////////////////////////////////////
/// Function compute the XOR function for two Statements.
///
/// \param a node's bdd_id.
/// \param b node's bdd_id..
/// \return The XOR function of two statement.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::xor2(const BDD_ID a, const BDD_ID b) {
	return ite(a, neg(b), b);
}

////////////////////////////////////////////////////////////////////////////////////
/// Function compute the NAND function for two Statements.
///
/// \param a node's bdd_id.
/// \param b node's bdd_id.
/// \return The NAND function of two statement.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::nand2(const BDD_ID a, const BDD_ID b) {
	return ite(a, neg(b), True());
}

////////////////////////////////////////////////////////////////////////////////////
/// Function compute the NOR function for two Statements.
///
/// \param a node's bdd_id.
/// \param b node's bdd_id.
/// \return The NOR function of two statement.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::nor2(const BDD_ID a, const BDD_ID b) {
	return ite(a, False(), neg(b));
}

////////////////////////////////////////////////////////////////////////////////////
/// Function is searching the name of the top variable.
///
/// \param bdd_id Unique id which is getting checked.
/// \return Name of the top variable.
////////////////////////////////////////////////////////////////////////////////////
std::string Manager::getTopVarName(const BDD_ID bdd_id) {

	checkBDD_IDRange(bdd_id);
	return uniqueTable[bdd_id].top;
}

////////////////////////////////////////////////////////////////////////////////////
/// Function finds the nodes of a BDD_ID root.
///
/// \param root Unique id which is getting checked.
/// \param notes_of_root Unique set of bdd_id which gets all nodes of the root.
/// \return void.
////////////////////////////////////////////////////////////////////////////////////
void Manager::findNodes(const BDD_ID &root, std::set<BDD_ID> &notes_of_root) {
	notes_of_root.insert(root);

	if (isConstant(root)) {
		return;
	}

	BDD_ID TE = highVar(root);
	BDD_ID FE = lowVar(root);

	findNodes(TE, notes_of_root);

	if (TE != FE) {
		findNodes(FE, notes_of_root);
	}
}

////////////////////////////////////////////////////////////////////////////////////
/// Function finds the nodes of a variable root.
///
/// \param root Unique id which is getting checked.
/// \param vars_of_root Unique set of bdd_id which gets all variables of the root.
/// \return void.
////////////////////////////////////////////////////////////////////////////////////
void Manager::findVars(const BDD_ID &root, std::set<BDD_ID> &vars_of_root) {
	BDD_ID root_var;

	if (isConstant(root)) {
		return;
	}

	root_var = topVar(root);

	for (auto node : uniqueTable) {
		if (root_var == topVar(node.first)) {
			vars_of_root.insert(node.first);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////
/// Function to verify if the ITE is a terminal case.
///
/// \param bdd_id Unique id which is getting checked.
/// \return Name of the top variable.
///////////////////////////////////////////////////////////////////////////////////
bool Manager::isTerminalCase(BDD_ID bdd_id_i, BDD_ID bdd_id_t, BDD_ID bdd_id_e,
		BDD_ID &bdd_id_terminalCase) {
	if (bdd_id_i == True())
		bdd_id_terminalCase = bdd_id_t;

	else if (bdd_id_i == False())
		bdd_id_terminalCase = bdd_id_e;

	else if (bdd_id_t == bdd_id_e)
		bdd_id_terminalCase = bdd_id_t;

	else if (bdd_id_t == True() && bdd_id_e == False())
		bdd_id_terminalCase = bdd_id_i;

	else
		return false;

	return true;
}
////////////////////////////////////////////////////////////////////////////////////
/// Function to verify if the ITE has been computed.
///
/// \param bdd_id Unique id which is getting checked.
/// \return False if the bdd_id has not been computed and True if it has.
///////////////////////////////////////////////////////////////////////////////////
bool Manager::isComputed(const BDD_ID i, const BDD_ID t, const BDD_ID e,
		BDD_ID &bdd_id_terminalCase) {
	Key keys = { i, t, e };
	auto search = computedTable.find(keys);

	if (search != computedTable.end()) {
		bdd_id_terminalCase = search->second;
		return true;
	}

	return false;
}
////////////////////////////////////////////////////////////////////////////////////
/// Function to add a new a bdd_id with its Then and Else bdd_id.
///
/// \param bdd_id Unique id which is getting checked.
/// \return void.
///////////////////////////////////////////////////////////////////////////////////
void Manager::addComputed(const BDD_ID i, const BDD_ID t, const BDD_ID e,
		const BDD_ID id) {
	Key keys = { i, t, e };

	computedTable.insert(std::make_pair(keys, id));

}

////////////////////////////////////////////////////////////////////////////////////
/// Function is throwing an exception for an invalid BDD_ID.
///
/// \param bdd_id Unique id which is invalid.
////////////////////////////////////////////////////////////////////////////////////
void Manager::checkBDD_IDRange(const BDD_ID bdd_id) {
	if (bdd_id < False() || uniqueTable.size() < bdd_id) {
		throw std::invalid_argument("BDD_ID doesn't exist");

	}
}

////////////////////////////////////////////////////////////////////////////////////
/// Function for inserting a new variable.
///
/// \param label Name of the variable to create.
/// \return unique id of new variable. 0 means the variable exists already.
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::makeNode(const std::string &label, BDD_ID bdd_id_high,
		BDD_ID bdd_id_low) {
	tripple *content = new tripple;
	BDD_ID bdd_id;

	if (isComputed(topVar(label), bdd_id_high, bdd_id_low, bdd_id)) {
		return bdd_id;
	}

	*content = {label, bdd_id_high, bdd_id_low};
	bdd_id = uniqueTable.size() + 1;

	uniqueTable[bdd_id] = *content;
	addComputed(topVar(label), bdd_id_high, bdd_id_low, bdd_id);

	return bdd_id;
}

////////////////////////////////////////////////////////////////////////////////////
/// Function is searching the bdd_id of the top variable.
///
/// \param bdd_id Unique id which is getting checked.
/// \return Id of the top variable. 0 means error
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::topVar(const BDD_ID bdd_id_f, const BDD_ID bdd_id_g,
		const BDD_ID bdd_id_h) {
	std::vector<BDD_ID> first;

	if (topVar(bdd_id_f) > True()) {
		first.push_back(topVar(bdd_id_f));
	}
	if (topVar(bdd_id_g) > True()) {
		first.push_back(topVar(bdd_id_g));
	}
	if (topVar(bdd_id_h) > True()) {
		first.push_back(topVar(bdd_id_h));
	}

	std::sort(std::begin(first), std::end(first));
	return first.front();
}
////////////////////////////////////////////////////////////////////////////////////
/// Function is searching the bdd_id of the top variable.
///
/// \param bdd_id Unique id which is getting checked.
/// \return Id of the top variable. 0 means error
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::topVar(const std::string &label) {
	std::map<BDD_ID, tripple>::iterator iterator;
	BDD_ID topVarBddId;

	topVarBddId = 0;

	for (iterator = uniqueTable.begin(); iterator != uniqueTable.end();
			++iterator) {
		if (iterator->second.top.compare(label) == 0) {
			topVarBddId = iterator->first;
			break;
		}
	}

	return topVarBddId;
}
////////////////////////////////////////////////////////////////////////////////////
/// Function provides the "High" part of a bdd_id
///
/// \param bdd_id Unique id which is getting checked.
/// \return Id of the top variable's HIGH bdd_id. 0 means error
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::highVar(const BDD_ID bdd_id) {
	return uniqueTable[bdd_id].high;
}
////////////////////////////////////////////////////////////////////////////////////
/// Function provides the "Low" part of a bdd_id
///
/// \param bdd_id Unique id which is getting checked.
/// \return Id of the top variable's LOW bdd_id. 0 means error
////////////////////////////////////////////////////////////////////////////////////
BDD_ID Manager::lowVar(const BDD_ID bdd_id) {
	return uniqueTable[bdd_id].low;
}
////////////////////////////////////////////////////////////////////////////////////
/// Function provides a String of a bdd_id, low and high components in the next format-> "{I,T,E}"
///
/// \param bdd_id Unique id which is getting checked.
/// \return String of the a bdd_id, low and high components. 0 means error
////////////////////////////////////////////////////////////////////////////////////
std::string Manager::printBDD_ID(const BDD_ID bdd_id) {
	BDD_ID result;
	if (isTerminalCase(bdd_id, highVar(bdd_id), lowVar(bdd_id), result)) {
		return " {" + getTopVarName(result) + ","
				+ getTopVarName(highVar(bdd_id)) + ","
				+ getTopVarName(lowVar(bdd_id)) + "} ";
	} else {
		std::string topVariable = getTopVarName(bdd_id);
		std::string highVariable, lowVariable;
		if (isTerminalCase(highVar(bdd_id), highVar(highVar(bdd_id)),
				lowVar(highVar(bdd_id)), result)) {
			highVariable = getTopVarName(result);
		} else {
			BDD_ID highVariableID = highVar(bdd_id);
			highVariable = printBDD_ID(highVariableID);
		}
		if (isTerminalCase(lowVar(bdd_id), highVar(lowVar(bdd_id)),
				lowVar(lowVar(bdd_id)), result)) {
			lowVariable = getTopVarName(result);
		} else {
			BDD_ID lowVariableID = lowVar(bdd_id);
			lowVariable = printBDD_ID(lowVariableID);
		}

		return " {" + topVariable + "," + highVariable + "," + lowVariable
				+ "} ";
	}
}
