/**
 * \brief ‫Implements methods of the ManagerTest class
 *
 * \author Oliver Griebel
 * \author Luis Humberto Nomura Quiroz
 */
#include "ManagerTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(ManagerTest);

////////////////////////////////////////////////////////////////////////////////////
///CppUnit: override setUp() to initialize the variables
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::setUp(void) {
	p_m = new Manager();
	Manager mgr1;
	Manager mgr2;
	Manager mgr3;
	Manager mgr4;
	Manager mgr5;
}

////////////////////////////////////////////////////////////////////////////////////
///CppUnit: override tearDown() to release any permanent resources you allocated in setUp()
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::tearDown(void) {
	delete p_m;
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the constructor
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::constructorTest(void) {
	Manager m;
	CPPUNIT_ASSERT_EQUAL(true, m.True() == (BDD_ID ) 2);
	CPPUNIT_ASSERT_EQUAL(true, m.False() == (BDD_ID ) 1);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the destructor
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::destructorTest(void) {

}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks createVar() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::createVarTest(void) {
	CPPUNIT_ASSERT(p_m->createVar("a") == 3);
	CPPUNIT_ASSERT(p_m->createVar("a") == 3);
	CPPUNIT_ASSERT(p_m->createVar("b") != 5);
	CPPUNIT_ASSERT(p_m->createVar("c") == 5);
	CPPUNIT_ASSERT(p_m->createVar("c") != 6);
	Manager m, simpleOR;
	BDD_ID a = m.createVar("a");
	CPPUNIT_ASSERT_EQUAL(true, a != BDD_UNDEF);
	CPPUNIT_ASSERT_EQUAL(true, m.True() != a);
	CPPUNIT_ASSERT_EQUAL(true, m.False() != a);
	CPPUNIT_ASSERT_EQUAL(false, m.isConstant(a));
	CPPUNIT_ASSERT_EQUAL(true, m.isVariable(a));

	BDD_ID b = m.createVar("b");
	CPPUNIT_ASSERT_EQUAL(true, b != BDD_UNDEF);
	CPPUNIT_ASSERT_EQUAL(true, m.True() != b);
	CPPUNIT_ASSERT_EQUAL(true, m.False() != b);
	CPPUNIT_ASSERT_EQUAL(false, m.isConstant(b));
	CPPUNIT_ASSERT_EQUAL(true, m.isVariable(b));

	CPPUNIT_ASSERT_EQUAL(true, a < b);
	BDD_ID bdd_a1 = m.createVar("a");
	BDD_ID bdd_a2 = m.createVar("a");
	CPPUNIT_ASSERT_EQUAL(bdd_a1, bdd_a2);

	BDD_ID ee = m.createVar("");
	CPPUNIT_ASSERT_EQUAL(ee, BDD_UNDEF);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the true() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::trueTest(void) {
	CPPUNIT_ASSERT(p_m->True() == 2);
	CPPUNIT_ASSERT(p_m->True() != 1);

	Manager m;
	BDD_ID bdd_true = m.True();

	CPPUNIT_ASSERT_EQUAL(m.True(), bdd_true);
	CPPUNIT_ASSERT_EQUAL(true, m.False() != bdd_true);
	CPPUNIT_ASSERT_EQUAL(bdd_true, (BDD_ID )2);
	CPPUNIT_ASSERT_EQUAL(true, bdd_true != BDD_UNDEF);
	CPPUNIT_ASSERT_EQUAL(true, bdd_true != (BDD_ID )1);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks false() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::falseTest(void) {
	CPPUNIT_ASSERT(p_m->False() == 1);
	CPPUNIT_ASSERT(p_m->False() != 2);

	Manager m;
	BDD_ID bdd_false = m.False();

	CPPUNIT_ASSERT_EQUAL(m.False(), bdd_false);
	CPPUNIT_ASSERT_EQUAL(true, m.True() != bdd_false);
	CPPUNIT_ASSERT_EQUAL(bdd_false, (BDD_ID )1);
	CPPUNIT_ASSERT_EQUAL(true, bdd_false != BDD_UNDEF);
	CPPUNIT_ASSERT_EQUAL(true, bdd_false != (BDD_ID )2);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the isConstant() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::isConstantTest(void) {
	CPPUNIT_ASSERT(p_m->isConstant(1) == true);
	CPPUNIT_ASSERT(p_m->isConstant(2) == true);
	BDD_ID id = p_m->createVar("a");
	CPPUNIT_ASSERT(p_m->isConstant(id) == false);

	Manager m;
	CPPUNIT_ASSERT_EQUAL(true, m.isConstant(m.False()));
	CPPUNIT_ASSERT_EQUAL(true, m.isConstant(m.True()));

	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	CPPUNIT_ASSERT_EQUAL(false, m.isConstant(a));
	CPPUNIT_ASSERT_EQUAL(false, m.isConstant(b));

	BDD_ID ab = m.and2(a, b);
	CPPUNIT_ASSERT_EQUAL(false, m.isConstant(ab));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the error of the isConstant() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::isConstantExceptionTest(void) {
	Manager m;
	BDD_ID wrong_id = 3;
	m.isConstant(wrong_id);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the isVariable() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::isVariableTest(void) {
	CPPUNIT_ASSERT(p_m->isVariable(p_m->False()) == false);
	CPPUNIT_ASSERT(p_m->isVariable(p_m->True()) == false);
	BDD_ID id = p_m->createVar("a");
	CPPUNIT_ASSERT(p_m->isVariable(id) == true);

	Manager m;
	CPPUNIT_ASSERT_EQUAL(false, m.isVariable(m.False()));
	CPPUNIT_ASSERT_EQUAL(false, m.isVariable(m.True()));

	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	CPPUNIT_ASSERT_EQUAL(true, m.isVariable(a));
	CPPUNIT_ASSERT_EQUAL(true, m.isVariable(a));

	BDD_ID ab = m.and2(a, b);
	CPPUNIT_ASSERT_EQUAL(false, m.isVariable(ab));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the error of the isVariable() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::isVariableExceptionTest(void) {
	Manager m;
	BDD_ID wrong_id = 3;
	m.isVariable(wrong_id);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the topVar() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::topVarTest(void) {
	CPPUNIT_ASSERT(p_m->topVar(1) == 1);
	CPPUNIT_ASSERT(p_m->topVar(2) == 2);
	BDD_ID idA = p_m->createVar("a");
	BDD_ID idB = p_m->createVar("b");
	CPPUNIT_ASSERT(p_m->topVar(idA) == idA);
	CPPUNIT_ASSERT(p_m->topVar(idA) != p_m->topVar(idB));

	Manager m;
	CPPUNIT_ASSERT_EQUAL(static_cast<BDD_ID>(2),
			static_cast<BDD_ID>(m.topVar(m.True())));
	CPPUNIT_ASSERT_EQUAL(static_cast<BDD_ID>(1),
			static_cast<BDD_ID>(m.topVar(m.False())));

	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	CPPUNIT_ASSERT_EQUAL(a, static_cast<BDD_ID>(m.topVar(a)));
	CPPUNIT_ASSERT_EQUAL(b, static_cast<BDD_ID>(m.topVar(b)));

	BDD_ID ab = m.and2(a, b);
	CPPUNIT_ASSERT_EQUAL(a, static_cast<BDD_ID>(m.topVar(ab)));
	CPPUNIT_ASSERT_EQUAL(b, static_cast<BDD_ID>(m.coFactorTrue(ab)));
	CPPUNIT_ASSERT_EQUAL(m.False(), static_cast<BDD_ID>(m.coFactorFalse(ab)));
	CPPUNIT_ASSERT_EQUAL(m.True(),
			static_cast<BDD_ID>(m.coFactorTrue(m.coFactorTrue(ab))));

}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the error of the topVar() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::topVarExceptionTest(void) {
	Manager m;
	BDD_ID wrong_id = 3;
	m.topVar(wrong_id);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the cofactorTrue() functions
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::coFactorTrueTest(void) {
	BDD_ID idA = p_m->createVar("a");
	BDD_ID idB = p_m->createVar("b");
	BDD_ID idC = p_m->createVar("c");
	BDD_ID idBandC = p_m->and2(idB, idC);
	BDD_ID idBorC = p_m->or2(idB, idC);

	CPPUNIT_ASSERT(p_m->coFactorTrue(idA) == p_m->True());
	CPPUNIT_ASSERT(p_m->coFactorTrue(idB) == p_m->True());

	CPPUNIT_ASSERT_EQUAL(p_m->coFactorTrue(idA, idA), p_m->True());
	CPPUNIT_ASSERT_EQUAL(p_m->coFactorTrue(idB, idA), idB);

	CPPUNIT_ASSERT_EQUAL(idBandC, p_m->coFactorTrue(idBandC, idA));
	CPPUNIT_ASSERT_EQUAL(idC, p_m->coFactorTrue(idBandC, idB));
	CPPUNIT_ASSERT_EQUAL(idB, p_m->coFactorTrue(idBandC, idC));

	CPPUNIT_ASSERT_EQUAL(idBorC, p_m->coFactorTrue(idBorC, idA));
	CPPUNIT_ASSERT_EQUAL(p_m->True(), p_m->coFactorTrue(idBorC, idB));
	CPPUNIT_ASSERT_EQUAL(p_m->True(), p_m->coFactorTrue(idBorC, idC));

	Manager mgr;

	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorTrue(mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.coFactorTrue(mgr.False()));
	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorTrue(mgr.True(), mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorTrue(mgr.True(), mgr.False()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(),
			mgr.coFactorTrue(mgr.False(), mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(),
			mgr.coFactorTrue(mgr.False(), mgr.False()));

	BDD_ID a = mgr.createVar("a");

	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorTrue(a));
	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.coFactorTrue(mgr.neg(a)));

	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorTrue(a, a));
	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.coFactorTrue(mgr.neg(a), a));

	BDD_ID b = mgr.createVar("b");

	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorTrue(b, a));

	BDD_ID f = mgr.and2(a, b);

	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorTrue(f));
	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorTrue(f, a));
	CPPUNIT_ASSERT_EQUAL(a, mgr.coFactorTrue(f, b));

	//
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the cofactorFalse() functions
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::coFactorFalseTest(void) {
	BDD_ID idA = p_m->createVar("a");
	BDD_ID idB = p_m->createVar("b");
	BDD_ID idC = p_m->createVar("c");
	BDD_ID idBandC = p_m->and2(idB, idC);
	BDD_ID idBorC = p_m->or2(idB, idC);

	CPPUNIT_ASSERT(p_m->coFactorFalse(idA) == p_m->False());
	CPPUNIT_ASSERT(p_m->coFactorFalse(idB) == p_m->False());

	CPPUNIT_ASSERT_EQUAL(p_m->False(), p_m->coFactorFalse(idA, idA));
	CPPUNIT_ASSERT_EQUAL(idB, p_m->coFactorFalse(idB, idA));

	CPPUNIT_ASSERT_EQUAL(idBandC, p_m->coFactorFalse(idBandC, idA));
	CPPUNIT_ASSERT_EQUAL(p_m->False(), p_m->coFactorFalse(idBandC, idB));
	CPPUNIT_ASSERT_EQUAL(p_m->False(), p_m->coFactorFalse(idBandC, idC));

	CPPUNIT_ASSERT_EQUAL(idBorC, p_m->coFactorFalse(idBorC, idA));
	CPPUNIT_ASSERT_EQUAL(idC, p_m->coFactorFalse(idBorC, idB));
	CPPUNIT_ASSERT_EQUAL(idC, p_m->coFactorFalse(idBorC, idC));

	Manager mgr;

	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorFalse(mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.coFactorFalse(mgr.False()));

	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorFalse(mgr.True(), mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.True(),
			mgr.coFactorFalse(mgr.True(), mgr.False()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(),
			mgr.coFactorFalse(mgr.False(), mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(),
			mgr.coFactorFalse(mgr.False(), mgr.False()));

	BDD_ID a = mgr.createVar("a");

	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.coFactorFalse(a));
	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorFalse(mgr.neg(a)));

	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.coFactorFalse(a, a));
	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.coFactorFalse(mgr.neg(a), a));

	BDD_ID b = mgr.createVar("b");

	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorFalse(b, a));

	BDD_ID f = mgr.or2(a, b);

	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorFalse(f));
	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorFalse(f, a));
	CPPUNIT_ASSERT_EQUAL(b, mgr.coFactorFalse(f, b));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the if-then-else() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::iteTest(void) {
	BDD_ID idI = p_m->createVar("i");
	BDD_ID idT = p_m->createVar("t");
	BDD_ID idE = p_m->createVar("e");
	BDD_ID idA = p_m->createVar("a");
	BDD_ID idB = p_m->createVar("b");

	CPPUNIT_ASSERT(p_m->ite(p_m->True(), idT, idE) == idT);
	CPPUNIT_ASSERT(p_m->ite(p_m->False(), idT, idE) == idE);
	CPPUNIT_ASSERT(p_m->ite(p_m->False(), idT, idT) == idT);
	CPPUNIT_ASSERT(p_m->ite(p_m->True(), idT, idT) == idT);
	CPPUNIT_ASSERT(p_m->ite(idI, p_m->True(), p_m->False()) == idI);

	BDD_ID a_b = p_m->or2(idA, idB);
	CPPUNIT_ASSERT(p_m->ite(idA, p_m->True(), p_m->False()) == idA);
	CPPUNIT_ASSERT(p_m->ite(idB, p_m->True(), p_m->False()) == idB);
	CPPUNIT_ASSERT(p_m->ite(a_b, p_m->True(), idB) == a_b);

	CPPUNIT_ASSERT(p_m->ite(a_b, idE, idE) == idE);

	Manager mgr;

	BDD_ID f = mgr.createVar("f");
	BDD_ID g = mgr.createVar("g");

	CPPUNIT_ASSERT_EQUAL(f, mgr.ite(f, mgr.True(), mgr.False()));
	CPPUNIT_ASSERT_EQUAL(mgr.True(), mgr.ite(f, mgr.True(), mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.False(), mgr.ite(f, mgr.False(), mgr.False()));
	CPPUNIT_ASSERT_EQUAL(f, mgr.ite(mgr.True(), f, g));
	CPPUNIT_ASSERT_EQUAL(f, mgr.ite(mgr.False(), g, f));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, mgr.True(), g), mgr.ite(f, f, g));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, mgr.True(), g), mgr.ite(g, mgr.True(), f));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, mgr.True(), g), mgr.ite(g, g, f));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, g, mgr.False()), mgr.ite(f, g, f));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, g, mgr.False()),
			mgr.ite(g, f, mgr.False()));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, g, mgr.True()), mgr.ite(f, g, mgr.neg(f)));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, g, mgr.True()),
			mgr.ite(mgr.neg(g), mgr.neg(f), mgr.True()));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, mgr.False(), g), mgr.ite(f, mgr.neg(f), g));
	CPPUNIT_ASSERT_EQUAL(mgr.ite(f, mgr.False(), g),
			mgr.ite(mgr.neg(g), mgr.False(), mgr.neg(f)));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the and2() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::and2Test(void) {
	CPPUNIT_ASSERT(p_m->and2(p_m->False(), p_m->False()) == p_m->False());
	CPPUNIT_ASSERT(p_m->and2(p_m->False(), p_m->True()) == p_m->False());
	CPPUNIT_ASSERT(p_m->and2(p_m->True(), p_m->False()) == p_m->False());
	CPPUNIT_ASSERT(p_m->and2(p_m->True(), p_m->True()) == p_m->True());

	BDD_ID idA = p_m->createVar("a");
	BDD_ID idB = p_m->createVar("b");
	CPPUNIT_ASSERT(p_m->coFactorTrue(p_m->and2(idA, idB)) == idB);
	CPPUNIT_ASSERT(p_m->coFactorFalse(p_m->and2(idA, idB)) == p_m->False());

	Manager mgr;
	BDD_ID a = mgr.createVar("a");
	BDD_ID b = mgr.createVar("b");
	BDD_ID f = mgr.and2(a, b);
	BDD_ID f_true = mgr.coFactorTrue(f);
	BDD_ID f_false = mgr.coFactorFalse(f);

	CPPUNIT_ASSERT_EQUAL(true, mgr.False() == f_false);
	CPPUNIT_ASSERT_EQUAL(true, b == f_true);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the or2() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::or2Test(void) {
	CPPUNIT_ASSERT(p_m->or2(p_m->False(), p_m->False()) == p_m->False());
	CPPUNIT_ASSERT(p_m->or2(p_m->False(), p_m->True()) == p_m->True());
	CPPUNIT_ASSERT(p_m->or2(p_m->True(), p_m->False()) == p_m->True());
	CPPUNIT_ASSERT(p_m->or2(p_m->True(), p_m->True()) == p_m->True());

	Manager m;
	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	BDD_ID c = m.createVar("c");

	BDD_ID a_b = m.or2(a, b);
	BDD_ID a_b_c = m.or2(a_b, c);

	CPPUNIT_ASSERT_EQUAL(a, (BDD_ID )m.topVar(a_b_c));
	CPPUNIT_ASSERT_EQUAL(m.True(), m.coFactorTrue(a_b));
	CPPUNIT_ASSERT_EQUAL(m.True(), m.coFactorTrue(a_b_c));
	CPPUNIT_ASSERT_EQUAL(m.True(),
			m.coFactorTrue(m.coFactorTrue(m.coFactorTrue(a_b_c))));
	CPPUNIT_ASSERT_EQUAL(m.False(), m.coFactorFalse(m.coFactorFalse(a_b)));
	CPPUNIT_ASSERT_EQUAL(c, m.coFactorFalse(m.coFactorFalse(a_b_c)));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the xor2() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::xor2Test(void) {
	CPPUNIT_ASSERT(p_m->xor2(p_m->False(), p_m->False()) == p_m->False());
	CPPUNIT_ASSERT(p_m->xor2(p_m->False(), p_m->True()) == p_m->True());
	CPPUNIT_ASSERT(p_m->xor2(p_m->True(), p_m->False()) == p_m->True());
	CPPUNIT_ASSERT(p_m->xor2(p_m->True(), p_m->True()) == p_m->False());

	Manager m;
	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	BDD_ID f = m.xor2(a, b);

	CPPUNIT_ASSERT_EQUAL(m.neg(b), m.coFactorTrue(f));
	CPPUNIT_ASSERT_EQUAL(b, m.coFactorFalse(f));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the not-equal() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::negTest(void) {
	CPPUNIT_ASSERT(p_m->neg(p_m->True()) == p_m->False());
	CPPUNIT_ASSERT(p_m->neg(p_m->False()) == p_m->True());

	Manager m;
	BDD_ID a = m.createVar("a");
	BDD_ID f = m.neg(a);

	CPPUNIT_ASSERT_EQUAL(m.False(), m.coFactorTrue(f));
	CPPUNIT_ASSERT_EQUAL(m.True(), m.coFactorFalse(f));
	CPPUNIT_ASSERT_EQUAL(a, m.neg(f));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the nand2() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::nand2Test(void) {
	CPPUNIT_ASSERT(p_m->nand2(p_m->False(), p_m->False()) == p_m->True());
	CPPUNIT_ASSERT(p_m->nand2(p_m->False(), p_m->True()) == p_m->True());
	CPPUNIT_ASSERT(p_m->nand2(p_m->True(), p_m->False()) == p_m->True());
	CPPUNIT_ASSERT(p_m->nand2(p_m->True(), p_m->True()) == p_m->False());

	Manager m;
	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	BDD_ID f = m.nand2(a, b);

	CPPUNIT_ASSERT_EQUAL(m.True(), m.coFactorFalse(f));
	CPPUNIT_ASSERT_EQUAL(m.neg(b), m.coFactorTrue(f));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the nor2() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::nor2Test(void) {
	CPPUNIT_ASSERT(p_m->nor2(p_m->False(), p_m->False()) == p_m->True());
	CPPUNIT_ASSERT(p_m->nor2(p_m->False(), p_m->True()) == p_m->False());
	CPPUNIT_ASSERT(p_m->nor2(p_m->True(), p_m->False()) == p_m->False());
	CPPUNIT_ASSERT(p_m->nor2(p_m->True(), p_m->True()) == p_m->False());

	Manager m;
	BDD_ID a = m.createVar("a");
	BDD_ID b = m.createVar("b");
	BDD_ID f = m.nor2(a, b);

	CPPUNIT_ASSERT_EQUAL(m.False(), m.coFactorTrue(f));
	CPPUNIT_ASSERT_EQUAL(m.neg(b), m.coFactorFalse(f));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the getTopVarName() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::getTopVarNameTest(void) {
	string nameA = "a";
	BDD_ID idA = p_m->createVar(nameA);

	CPPUNIT_ASSERT(p_m->getTopVarName(p_m->False()) == "0");
	CPPUNIT_ASSERT(p_m->getTopVarName(p_m->True()) == "1");
	CPPUNIT_ASSERT(p_m->getTopVarName(idA) == nameA);

	Manager m;
	BDD_ID d = m.createVar("d");
	BDD_ID a = m.createVar("a");
	BDD_ID c = m.createVar("c");
	BDD_ID b = m.createVar("b");
	BDD_ID da = m.and2(d, a);
	BDD_ID c_b = m.or2(c, b);

	CPPUNIT_ASSERT_EQUAL(std::string("d"), m.getTopVarName(da));
	CPPUNIT_ASSERT_EQUAL(std::string("c"), m.getTopVarName(c_b));

	BDD_ID f = m.xor2(da, c_b);
	CPPUNIT_ASSERT_EQUAL(std::string("d"), m.getTopVarName(f));
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the error of the getTopVarName() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::getTopVarNameExceptionTest(void) {
	Manager m;

	BDD_ID wrong_id = 3;
	m.isVariable(wrong_id);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the findNodes() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::findNodesTest(void) {
	Manager mgr;
	std::set<BDD_ID> nodes;
	std::set<BDD_ID> expected_nodes;

	BDD_ID a = mgr.createVar("a");
	BDD_ID b = mgr.createVar("b");
	BDD_ID c = mgr.createVar("c");
	BDD_ID e = mgr.or2(b, c);
	BDD_ID f = mgr.and2(a, e);

	mgr.findNodes(mgr.False(), nodes);
	expected_nodes.insert(mgr.False());
	CPPUNIT_ASSERT(expected_nodes == nodes);

	mgr.findNodes(e, nodes);
	expected_nodes.clear();
	expected_nodes.insert(e);
	expected_nodes.insert(c);
	expected_nodes.insert(mgr.True());
	expected_nodes.insert(mgr.False());
	CPPUNIT_ASSERT(expected_nodes == nodes);

	mgr.findNodes(f, nodes);
	expected_nodes.clear();
	expected_nodes.insert(f);
	expected_nodes.insert(e);
	expected_nodes.insert(c);
	expected_nodes.insert(mgr.True());
	expected_nodes.insert(mgr.False());
	CPPUNIT_ASSERT(expected_nodes == nodes);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the findVars() function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::findVarsTest(void) {
	Manager mgr;
	std::set<BDD_ID> nodes;
	std::set<BDD_ID> expected_nodes;

	BDD_ID a = mgr.createVar("a");
	BDD_ID b = mgr.createVar("b");
	BDD_ID c = mgr.createVar("c");
	BDD_ID e = mgr.or2(b, c);
	BDD_ID f = mgr.and2(a, e);

	mgr.findVars(mgr.False(), nodes);
	expected_nodes.clear();
	CPPUNIT_ASSERT(expected_nodes == nodes);

	nodes.clear();
	mgr.findVars(a, nodes);
	expected_nodes.clear();
	expected_nodes.insert(a);
	expected_nodes.insert(f);
	CPPUNIT_ASSERT(expected_nodes == nodes);

	nodes.clear();
	mgr.findVars(b, nodes);
	expected_nodes.clear();
	expected_nodes.insert(b);
	expected_nodes.insert(e);
	CPPUNIT_ASSERT(expected_nodes == nodes);

	nodes.clear();
	mgr.findVars(c, nodes);
	expected_nodes.clear();
	expected_nodes.insert(c);
	CPPUNIT_ASSERT(expected_nodes == nodes);
}

////////////////////////////////////////////////////////////////////////////////////
/// Method for Unit Test: checks the printBDD_ID function
////////////////////////////////////////////////////////////////////////////////////
void ManagerTest::printBDD_IDTest(void) {
	Manager mgr;
	BDD_ID a = mgr.createVar("a");
	BDD_ID b = mgr.createVar("b");
	BDD_ID c = mgr.createVar("c");
	BDD_ID d = mgr.createVar("d");
	BDD_ID F = mgr.or2(a, b);
	BDD_ID G = mgr.and2(a, c);
	BDD_ID H = mgr.or2(b, d);
	BDD_ID ITE = mgr.ite(F, G, H);
	CPPUNIT_ASSERT(" {a,c, {b,0,d} } " == mgr.printBDD_ID(ITE));
}
