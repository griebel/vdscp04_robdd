/**
 * \class ManagerTest
 *
 * \brief Tests the Manager class
 *
 * Fixture class containing the Unit Tests used to test the Manager class.
 * The test framework CppUnit is used.
 * This class inherits from TestFixture base class defined in the test framework.
 *
 * Read explanatory comments in the code.
 *
 * For more details see: http://cppunit.sourceforge.net/doc/cvs/cppunit_cookbook.html
 *
 * \author Oliver Griebel
 * \author Luis Humberto Nomura Quiroz
 *
 */
#ifndef MANAGERTEST_HPP
#define MANAGERTEST_HPP

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "../Manager.hpp"

using namespace std;
using namespace CppUnit;

class ManagerTest: public TestFixture {
CPPUNIT_TEST_SUITE(ManagerTest);
	CPPUNIT_TEST(constructorTest);
	CPPUNIT_TEST(destructorTest);
	CPPUNIT_TEST(createVarTest);
	CPPUNIT_TEST(trueTest);
	CPPUNIT_TEST(falseTest);
	CPPUNIT_TEST(isConstantTest);
	CPPUNIT_TEST_EXCEPTION(isConstantExceptionTest, std::invalid_argument);
	CPPUNIT_TEST(isVariableTest);
	CPPUNIT_TEST_EXCEPTION(isVariableExceptionTest, std::invalid_argument);
	CPPUNIT_TEST(topVarTest);
	CPPUNIT_TEST_EXCEPTION(topVarExceptionTest, std::invalid_argument);
	CPPUNIT_TEST(coFactorTrueTest);
	CPPUNIT_TEST(coFactorFalseTest);
	CPPUNIT_TEST(iteTest);
	CPPUNIT_TEST(and2Test);
	CPPUNIT_TEST(or2Test);
	CPPUNIT_TEST(xor2Test);
	CPPUNIT_TEST(negTest);
	CPPUNIT_TEST(nand2Test);
	CPPUNIT_TEST(nor2Test);
	CPPUNIT_TEST(getTopVarNameTest);
	CPPUNIT_TEST_EXCEPTION(getTopVarNameExceptionTest, std::invalid_argument);
	CPPUNIT_TEST(findNodesTest);
	CPPUNIT_TEST(findVarsTest);
	CPPUNIT_TEST(printBDD_IDTest);CPPUNIT_TEST_SUITE_END()
	;

public:
	void setUp(void);
	void tearDown(void);
	void constructorTest(void);
	void destructorTest(void);
	void createVarTest(void);
	void trueTest(void);
	void falseTest(void);
	void isConstantTest(void);
	void isConstantExceptionTest(void);
	void isVariableTest(void);
	void isVariableExceptionTest(void);
	void topVarTest(void);
	void topVarExceptionTest(void);
	void coFactorTrueTest(void);
	void coFactorFalseTest(void);
	void iteTest(void);
	void and2Test(void);
	void or2Test(void);
	void xor2Test(void);
	void negTest(void);
	void nand2Test(void);
	void nor2Test(void);
	void getTopVarNameTest(void);
	void getTopVarNameExceptionTest(void);
	void findNodesTest(void);
	void findVarsTest(void);
	void printBDD_IDTest(void);

private:
	Manager *p_m;
	const BDD_ID BDD_UNDEF = static_cast<BDD_ID>(0);
};

#endif /* MANAGERTEST_HPP */
