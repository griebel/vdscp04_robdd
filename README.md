﻿# Verification of digital Systems Lab

###### by Oliver Griebel and Luis Humberto Nomura

## Questions

### How to build a static library from your source. (e.g.: make Makefile or scons -u ...)

Makefiles are written so that if you run "make" with no arguments, it does just that.

The exit status of make is always one of three values:

0-  The exit status is zero if make is successful. 

1-  The exit status is one if you use the ‘-q’ flag and make determines that some target is not already up to date

2-  The exit status is two if make encounters any errors. It will print messages describing the particular errors. 

### How to run the unittests.

Subclass the TestCase class. Override the method runTest(). When you want to check a value, call CPPUNIT_ASSERT(bool) and pass in an expression that is true if the test succeeds. 

### How to run the acceptance tests of the entire package.

A fixture is a known set of objects that serves as a base for a set of test cases. 
Fixtures come in very handy when you are testing as you develop. 

## Commands

### How to build the project

using Makefile in root folder

`make all`

using SCons in root folder without unit tests

`scons`

using SCons in root folder with unit tests

`scons test`
		
### How to get the project

Switch to an empty folder using one of the commands

`git clone https://github.com/olgr/vdscp04_robdd.gitt .`

`git checkout https://github.com/olgr/vdscp04_robdd.git .`

### How to update the local project to master branch

`git checkout FILENAME`

### How to remove a file from the staging area

`git reset FILENAME`

### How to commit data

Add changed files

`git add FILENAME`

Regular checkout

`git commit -m "FILL IN YOUR COMMIT MESSAGE"`

Using a clone

`git commit -m "FILL IN YOUR COMMIT MESSAGE"`

`git push`

### How to create the documentation

Switch to doc/ folder and use command

`doxygen doxyConfigEx`
